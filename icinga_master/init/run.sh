#!/bin/bash


###############################################################################
# Initialize and start incinga2
#
# /icinga2 is mapped as an external volume holding the current configuration
#
# Based on
#-> https://github.com/psi-4ward/docker-icinga2/blob/master/rootfs/init/run.sh

set -e

# Make sure directories exists (in the volume)
#mkdir -p /icinga2/cmd
#mkdir -p /icinga2/conf
#mkdir -p /icinga2/ssh
#mkdir -p /icinga2/lib/api/log
#mkdir -p /var/run/icinga2
mkdir -p /run/icinga2

# Create symlinks
#rm -rf /var/run/icinga2/cmd /etc/icinga2 /var/spool/icinga2/.ssh /var/lib/icinga2
#ln -sf /icinga2/cmd /var/run/icinga2/cmd
#ln -sf /icinga2/ssh /var/spool/icinga2/.ssh
rm -rf /etc/icinga2 /etc/ssmtp
ln -sf /icinga2/etc/icinga2 /etc/icinga2
ln -sf /icinga2/etc/ssmtp /etc/ssmtp
#Mit einem Symlink auf ein externes Volume kann Docker später die Rechte nicht anpassen!
#ln -sf /icinga2/var/lib/icinga2 /var/lib/icinga2
cp -r /icinga2/var/lib/icinga2/* /var/lib/icinga2

# Copy default config (if volume is empty)
#if [ ! -e /icinga2/conf/icinga2.conf ] ; then
#  cp -r /temp/icinga2/* /icinga2/conf
#fi

# Run setups
[ -x /init/set-timezone.sh ] && /init/set-timezone.sh
[ -x /init/mysql_setup.sh ] && /init/mysql_setup.sh
[ -x /init/feature_setup.sh ] && /init/feature_setup.sh

# Fix ownership
#chown icinga /icinga2 -R
#chown nagios /etc/icinga2 -R
#chown icinga /var/run/icinga2 -R
#chown nagios /var/run/icinga2 -R
chown nagios /run/icinga2 -R
chown -R nagios:nagios /var/lib/icinga2

# Generate ssh-key for icinga2 user
#if [ ! -e /icinga2/ssh/id_rsa ] ; then
#  su icinga -c 'ssh-keygen -q -t rsa -N "" -f /icinga2/ssh/id_rsa'
#fi
#if [ ! -e /icinga2/ssh/id_rsa ] ; then
#  su nagios -c 'ssh-keygen -q -t rsa -N "" -f /icinga2/ssh/id_rsa'
#fi

# Run Icinga2 daemon
echo 'Start Icinga2 Daemon'
#exec dumb-init -- su icinga -c \
#  "icinga2 daemon --log-level ${ICINGA_LOGLEVEL:-warning} --include /etc/icinga2"
exec dumb-init -- su nagios -c \
  "/usr/sbin/icinga2 daemon --log-level ${ICINGA_LOGLEVEL:-warning} --include /etc/icinga2"
