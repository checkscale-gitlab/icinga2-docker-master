# Icinga2 Docker


## .env
Global configuration-parameters for the Docker-Containers

## conf
configuration-files for the containers. Each container has a separate subdirectory where the current configuration is loaded at startup of the container from.
This folder is mounted as an external volume to the Docker-Container so that changes to the configuration can be made from the environment hosting the docker-instance. Also the changes from inside the container are reflected back, making a backup of the current configuration simpler.

## master:
Docker-Image containing an icinga2-process working as a master of a cluster

## icingaweb2:
Docker-Image containing an icingaweb2-instance that is connected to the icinga-master


## Credits
This work is based on  
Bodo Schulz: https://github.com/bodsch/docker-icinga2
Christoph Wiechert: https://github.com/psi-4ward/docker-icinga2
